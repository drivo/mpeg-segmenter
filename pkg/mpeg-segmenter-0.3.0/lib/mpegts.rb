#!/usr/local/bin/ruby -w
# -*- coding: utf-8 -*-
# 
# Author:: Guido D'Albore
# Date:: 2012-01-02
# Version:: 0.3.0
# 
# Reference Specifications:
# 
#  * ISO 13818-1 (MPEG-part1)
#  * IETF Draft HTTP Live Streaming

$LOAD_PATH << File.dirname(__FILE__) unless $LOAD_PATH.include?(File.dirname(__FILE__))

require 'mpegts/mpegts_constants'
require 'mpegts/mpegts_file'
require 'mpegts/mpegts_segment'
require 'mpegts/httpls/httpls_m3u8'
require 'mpegts/httpls/httpls_segmenter'

module MPEGTS
    Version         = '0.3.0'
    Revision        = '0'
    RevisionDate    = "2011-12-30"
    
    InfoLog         = true
    WarnLog         = true
    ErrorLog        = true
    DebugLog        = true
end

###############################
# STATIC METHODS
###############################

def printUsage
    puts "MPEG Transport Segmenter v#{MPEGTS::Version}, rev. #{MPEGTS::Revision}"
    puts "Copyright (C) 2011 by Guido D'Albore (guido@bitstorm.it)"
    puts
    puts "Usage:"
    puts "\t" + File.split($0)[1] + " <input-ts-file> <duration-in-seconds> <output-path> [segment-base-URL]"
    puts
end

###############################
# CLI
###############################

ARGV[0] = "/media/MyOffice500/Dropbox/Progetti/GitHub.com/mpeg-ts/test/TheSimpsons.ts";
ARGV[1] = "10";
ARGV[2] = "/media/MyOffice500/Dropbox/Progetti/GitHub.com/mpeg-ts/test/output";

if((ARGV.length < 3) || (ARGV.length > 4))
    printUsage
    exit!
else
    input_file = ARGV[0] # Input .TS filepath
    input_file = input_file.sub("//", "/")
    input_file = input_file.sub("//", "/")
    output_basename = File.basename(input_file, File.extname(input_file))
    duration_in_seconds = ARGV[1].to_i
    output_folder = ARGV[2] + "/"
    base_url = ARGV[3] # it can be "nil"
    base_url = "" if(base_url.nil?)
    destination_file = output_folder + output_basename + "%d.ts"
end

begin
  segmenter = HTTPLiveStreaming::Segmenter::new(input_file, destination_file, output_folder, duration_in_seconds, base_url, output_basename)
  segmenter.process
rescue => exception
  printUsage
  puts exception
  exit!
end
