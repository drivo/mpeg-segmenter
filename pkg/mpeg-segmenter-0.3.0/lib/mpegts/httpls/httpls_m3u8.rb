module HTTPLiveStreaming
  class M3u8
      def initialize(filepath, targetDuration = nil, urlPrefix = nil, mediaSequence = nil)
          @file = File.open(filepath, "w")
          @file.puts("#EXTM3U")
          @file.puts("#EXT-X-TARGETDURATION:%d" % targetDuration) if(!targetDuration.nil?)
          @file.puts("#EXT-X-MEDIA-SEQUENCE:%d" % mediaSequence) if(!mediaSequence.nil?)
          @file.flush

          @duration = targetDuration
          
          @urlPrefix = urlPrefix
          @urlPrefix = "segment_" if(urlPrefix.nil?)
          @counter = 1
      end

      def insert(resourceUrl, duration = @duration, description = "")
          @file.puts(sprintf("#EXTINF:%d, %s", duration, description))
          @file.puts(resourceUrl)
      end

      def insertMedia(duration = nil)
          duration = @duration if(duration.nil?)

          insert(@urlPrefix + @counter.to_s + ".ts", duration)
          @counter += 1
      end

      def insertPlaylist(url, bandwidth, programId = 1, resolutionWidth = nil, resolutionHeight = nil)
          if(resolutionWidth.nil? || resolutionHeight.nil?)
              @file.puts(sprintf("#EXT-X-STREAM-INF:PROGRAM-ID=%d, BANDWIDTH=%d", programId, bandwidth))
          else
              @file.puts(sprintf("#EXT-X-STREAM-INF:PROGRAM-ID=%d, BANDWIDTH=%d, RESOLUTION=%dx%d", programId, bandwidth, resolutionWidth, resolutionHeight))
          end

          @file.puts(url)
      end

      def close
          @file.puts("#EXT-X-ENDLIST")
          @file.close
      end
  end
end

__END__

** Usage examples **
#
#playlist = M3u8.new("/test-low.m3u8", 15, "fragment_low_")
#playlist.insertMedia
#playlist.insertMedia
#playlist.insertMedia
#playlist.close
#
#playlist = M3u8.new("/test-hi.m3u8", 15, "fragment_hi_")
#playlist.insertMedia
#playlist.insertMedia
#playlist.insertMedia
#playlist.close
#
#playlist = M3u8.new("/test_variants.m3u8")
#playlist.insertPlaylist("test-low.m3u8", 250000, 1, 400, 224)
#playlist.insertPlaylist("test-hi.m3u8", 1240000, 1, 640, 360)
#playlist.close

** Example M3U8 with bandwidth adaptation **
---
##EXTM3U
#EXT-X-STREAM-INF:PROGRAM-ID=1, BANDWIDTH=240000
iphone_3g/stream.m3u8
#EXT-X-STREAM-INF:PROGRAM-ID=1, BANDWIDTH=640000
iphone_wifi/stream.m3u8
#EXT-X-STREAM-INF:PROGRAM-ID=1, BANDWIDTH=1240000
ipad_wifi/stream.m3u8
---

** Example M3U8 with single segmented media **
---
#EXTM3U
#EXT-X-TARGETDURATION:10
#EXT-X-MEDIA-SEQUENCE:0
#EXTINF:10, no desc
fileSequence1.ts
#EXTINF:10, no desc
fileSequence2.ts
#EXTINF:10, no desc
fileSequence3.ts
#EXTINF:10, no desc
fileSequence4.ts
#EXTINF:10, no desc
fileSequence5.ts
#EXTINF:10, no desc
fileSequence6.ts
#EXTINF:10, no desc
fileSequence7.ts
#EXTINF:10, no desc
fileSequence8.ts
#EXTINF:10, no desc
fileSequence9.ts
#EXTINF:10, no desc
fileSequence10.ts
#EXT-X-ENDLIST
---