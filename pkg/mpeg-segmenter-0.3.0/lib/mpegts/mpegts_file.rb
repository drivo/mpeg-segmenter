#require 'mpegts/mpegts_constants'
#require 'mpegts/mpegts_segment'

module MPEGTS
  class MpegTsFile
    def initialize(tsFilePath)
        raise "File \"#{tsFilePath}\" doesn't exist!" if !File.exist?(tsFilePath)

        @filePath = tsFilePath
        @fileSize = File.size(filePath)

        raise "File \"#{tsFilePath}\" contains an incorrent size (it must be multiple of 188 bytes)." if self.size%MPEGTS::MPEGTSConstants::TS_PACKET_SIZE != 0

      
      @segmentCount = File.size(filePath) / MPEGTS::MPEGTSConstants::TS_PACKET_SIZE

        @currentSegment = 0

        # It's better to pre-open the file
        # otherwise the time to browse the segment
        # increases hugely
        #@file = File.open(@filePath, "r:binary")
        @file = File.open(@filePath, "rb")
    end

    # Getters
    def filePath; @filePath; end
    def segmentCount; @segmentCount; end
    def size; @fileSize ; end
    def currentSegment; @currentSegment; end

    def eachSegment(&b)
        @currentSegment = 0;

        0.upto(@segmentCount-1) {
            b.call(nextSegment)
        }
    end

    def nextSegment()
        if(@currentSegment == @segmentCount)
            # We reached the end of file
            return nil
        end

        segment = getSegment(@currentSegment)
        @currentSegment += 1

        return segment
    end

    def getSegment(segmentNumber)
        if((segmentNumber < 0) || (segmentNumber >= @segmentCount))
            return nil
        end

        @file.seek(MPEGTS::MPEGTSConstants::TS_PACKET_SIZE * segmentNumber, IO::SEEK_SET)
        data = @file.read(MPEGTS::MPEGTSConstants::TS_PACKET_SIZE)

        MPEGTSSegment.new(data.bytes.to_a, segmentNumber)
    end

  end
end
